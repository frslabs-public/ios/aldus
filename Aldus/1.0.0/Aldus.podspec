#
# Be sure to run `pod lib lint Torus.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Aldus'
  s.version          = '1.0.0'
  s.summary          = 'Aldus is used for Video Calling'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/aldus'
  s.license          = 'MIT'
  s.author           = { 'ashish' => 'ashish@frslabs.com' }
  s.source           = { :http => 'https://aldus-ios.repo.frslabs.space/aldus-ios/1.0.0/Aldus.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '12.0'
  s.ios.vendored_frameworks = 'Aldus.framework'
  s.swift_version = '5.0'
end
